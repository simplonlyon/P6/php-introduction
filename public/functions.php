<?php

// firstFunction(10);

/**
 * Les fonctions en PHP se déclarent et s'utilisent de la même
 * manière qu'en Javascript à une exception près : 
 * On peut indiquer le type des paramètres attendu et le type 
 * renvoyé par la fonction.
 * Ici, notre fonction attend un paramètre de type integer en 
 * premier paramètre et doit avoir un return de string.
 * Si l'une de ces deux choses ne sont pas respectées, PHP plantera
 */
function firstFunction(int $param): string {

    echo $param;
    
    return "bloup";
}


/**
 * faire une fonction search qui va attendre 2 arguments : 
 * un tableau (array) et un terme à chercher (string)
 * Cette fonction va parcourir les éléments du tableau (voir foreach)
 * et si elle trouve le terme voulu, elle renvoie son index, sinon
 * elle renvoie null.
 * Typer la fonction au maximum
 */
$tab = ["zo", "bu", "ga"];
echo search($tab, "bu");
//On met un ? devant le type de retour pour indiquer à PHP que 
//cette fonction pourra renvoyer soit de l'int soit null 
//(null représentant dans les langages de programmation l'absence de valeur)
function search(array $paramTab, string $toSearch): ?int {
    //On parcours le tableau en récupérant valeur et clef/index
    foreach($paramTab as $index => $value) {
        //Si jamais la valeur actuelle correspond au terme recherché
        if($value === $toSearch) {
            //on return l'index auquel on l'a trouvé
            return $index;
        }
    }
    //Si la boucle se termine sans avoir fait de return, c'est que
    //le terme n'est pas dans le tableau, alors on renvoie null
    return null;
}