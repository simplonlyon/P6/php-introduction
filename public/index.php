<?php

/**
 * Les variables en PHP doivent être précédées d'un $
 * Pour déclarer une nouvelle variable on met juste le nom de celle
 * ci avec son dollar et sa valeur (ou pas si pas de valeur par défaut)
 */
$maVariable = 10;

//Les if sont les même qu'en JS
if($maVariable === 10) {
    
    //mais il existe un mot clef elseif, si on aime pas les espaces
} elseif ($maVariable > 12) {

} else {

}

//Switch identique au JS
switch ($maVariable) {
    case 10:
        //fait des trucs sur maVariable vaut 10
        break;
    
    default:
        //Fait des trucs, si on tombe dans aucun des cases
        break;
}