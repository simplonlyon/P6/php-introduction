<?php
/**
 * En PHP, on peut ajouter le contenu de n'importe quel fichier dans
 * un autre. Cela rendra accessible toutes les variables/fonctions
 * classes ou autres déclarées dans cette autre fichier dans celui
 * qui include. 
 * Il y a plusieurs manières d'include un fichier : 
 */
//Avec require, si le fichier ne peut pas être inclus, PHP plante et stop l'exécution du code
require "../src/fragment.php";
//Avec include, si le fichier ne peut pas être inclus, PHP met un warning mais le code continue son exécution
// include "../src/fragment.php";
//require_once et include_once permettent de faire que si un fichier a déjà été inclus ailleurs, il ne sera pas re-inclus
// require_once "../src/fragment.php";
// include_once "../src/fragment.php";

echo "fichier include.php";
//Ici, on fait un echo d'une variable déclarée dans le fichier fragment.php (ce qui n'est pas top)
echo $uneVariable;
