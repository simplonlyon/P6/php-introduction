<?php
/**
 * Pour utiliser un élément venant d'un autre namespace, il faut
 * l'indiquer explicitement au fichier avec le mot clef use, suivit
 * du namespace+le nom de la classe qu'on va utiliser
 */
use App\Classes\First;
use App\Classes\Second;

//Pour que les fichiers contenant les classes soient require automatiquement
//par composer, on doit uniquemnet require le fichier autoload.php
require "../vendor/autoload.php";

//Vu qu'on a utilisé un use, on peut directement utilisé la classe
//normalement.
$instance = new First(10);
/*Alternativement, on aurait pu ne pas mettre de use et écrire à la place :
$instance = new App\Classes\First(10);

*/
//echo $instance->prop2; //ça déclencherait une erreur, car propriété privée
echo $instance->method();//ça par contre, c'est ok

$instanceSecond = new Second($instance);


var_dump($instanceSecond);