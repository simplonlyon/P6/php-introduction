<?php

//déclaration et utilisation des tableaux similaire au JS
$tab = ["ga", "zo", "bu", "meu"];
$tab[1] = "zobu";
// echo $tab[0];

//équivaut à un tab.push("autre") du javascript
$tab[] = "autre";
array_push($tab, "valeur");
//équivaut plus ou moins en js à faire des objets avec {prop:"valeur" ...}
$associatif = ["prop" => "valeur", "prop2" => 10];
echo $associatif["prop"];

//équivaut au tab.length du javascript
count($tab);




//Une boucle sur un tableau, comme en javascript
for ($index=0; $index < count($tab); $index++) { 
    //Si on utilise les guillemets, on peut mettre des variable 
    //directement dedans (un peu comme avec les backquote du js)
    echo "<p>$tab[$index]</p>";
}

//équivaut à un for(let item of tab) {} en javascript
//On peut également accéder à la clef/l'index actuel avec une fat arrow
foreach($tab as $key => $value) {
    //exemple de concaténation si on utilise des simples quotes.
    //Il faudra utiliser un . à la place du + de javascript
    echo '<p>'.$key . $value.'</p>';
}
// $counter = 0;
// while ($counter < 10) {
//     echo "while";
//     $counter++;
// }