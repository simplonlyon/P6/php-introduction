<?php

namespace App\Interfaces;
/**
 * Une interface est un des types les plus abstraits de la POO.
 * Elle ressemble vaguement à une classe mais ne contient que des
 * "méthode abstraites", c'est-à-dire des méthodes dont on a que
 * la définition (le nom de la méthode, ses arguments et leur type si
 * elle en a, son type de retour).
 * L'interface sera surtout utilisée pour le typage, pour dire par
 * exemple qu'une fonction attend en argument quelque chose qui 
 * correspond à l'interface Alive ou autre. On pourra alors donner
 * à cette fonction n'importe quelle classe tant que celle ci implémente
 * l'interface Alive.
 */
interface Alive {

    function breath(): void;

    function eat($food): void;
    
    function emitSound(): string;
}