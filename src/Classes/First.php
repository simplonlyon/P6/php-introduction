<?php
/**
 * Une classe "doit" avoir un namespace (en vrai c'est pas obligé mais
 * c'est une convention largement suivie).
 * Le namespace est plus ou moins l'équivalent des modules en JS,
 * avec webpack, chaque fichier était un module, en PHP, il faut 
 * déclarer le namespace manuellement pour dire qui à accès à quoi.
 * Les éléments (classes, interfaces, variables) d'un même namespace
 * auront accès les uns aux autres.
 * 
 * Par convention un namespace est composé de :
 * Le nom du namespace de l'application, ici App\
 * Le ou les dossiers dans lequel se trouve le fichier séparés par
 * des \ , ici Classes
 * 
 * On écrira les noms de dossiers et le nom de fichier en camel
 * case capitalisés dans les projets PHP pour qu'ils correspondent
 * au nom de classe et au namespace
 */
namespace App\Classes;
/**
 * La declaration des classes est similaire en PHP et en JS à la
 * différence que les propriétés peuvent être déclarées directement
 * dans le corps de la classe au lieu du constructeur (mais si ce
 * sont des propriétés public, ça n'est pas obligatoire)
 */
class First {
    //Quand on déclare une propriété dans le corps de la classe, on
    //doit indiquer sa visibilité (public, private, protected), son
    //nom, et éventuellement sa valeur
    //public : accessible depuis la classe et l'extérieur
    public $prop;
    //private : accessible uniquement à l'intérieur de la classe
    private $prop2;
    //protected : accessible uniquement à l'intérieur de la classe ou de ses enfants
    protected $propTected = "bloup";

    public function __construct(int $prop2) {
        $this->prop = "bloup";
        $this->prop2 = $prop2;
    }

    public function method() {
        //On peut accéder à prop2 ici, car on est dans la classe
        echo $this->prop2;
    }
}